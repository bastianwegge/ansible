#!/bin/bash -e

echo "cd into directory"
cd ./app

echo "pull new status"
git pull && npm install

echo "cd out of dir"
cd ..

echo "create tarball gapp.tar.gz"
tar cvfz ./gapp.tar.gz ./app

echo "move tarball into ansible/roles/web/files/gapp.tar.gz"
mv gapp.tar.gz ./roles/web/files/gapp.tar.gz
